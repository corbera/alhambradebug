{
  "version": "1.2",
  "package": {
    "name": "ALU rMIPs",
    "version": "0.1",
    "description": "ALU para el procesador rMIPS",
    "author": "Francisco Corbera",
    "image": "%3Csvg%20xmlns=%22http://www.w3.org/2000/svg%22%20width=%22143.071%22%20height=%22224.643%22%3E%3Cpath%20d=%22M1.143%2079.071V.5l141.375%2081.623V140.5l.053-2.857%22%20fill=%22none%22%20stroke=%22#000%22%20stroke-width=%224%22/%3E%3Cpath%20d=%22M1.143%20145.571v78.572l141.375-81.623V84.143l.053%202.857%22%20fill=%22none%22%20stroke=%22#000%22%20stroke-width=%224%22/%3E%3Cpath%20d=%22M.5%2078.357l59.062%2034.1-58.348%2033.687%22%20fill=%22none%22%20stroke=%22#000%22%20stroke-width=%224%22/%3E%3Ctext%20style=%22line-height:125%25;-inkscape-font-specification:Sans%20Bold;text-align:start%22%20x=%22201.786%22%20y=%22313.076%22%20font-size=%2218%22%20font-weight=%22700%22%20letter-spacing=%220%22%20word-spacing=%220%22%20font-family=%22Sans%22%20writing-mode=%22lr%22%20transform=%22translate(-196.643%20-263.29)%22%3E%3Ctspan%20x=%22201.786%22%20y=%22313.076%22%3EA%3C/tspan%3E%3C/text%3E%3Ctext%20style=%22line-height:125%25;-inkscape-font-specification:Sans%20Bold;text-align:start%22%20x=%22199.643%22%20y=%22448.434%22%20font-size=%2218%22%20font-weight=%22700%22%20letter-spacing=%220%22%20word-spacing=%220%22%20font-family=%22Sans%22%20writing-mode=%22lr%22%20transform=%22translate(-196.643%20-263.29)%22%3E%3Ctspan%20x=%22199.643%22%20y=%22448.434%22%3EB%3C/tspan%3E%3C/text%3E%3Ctext%20style=%22text-align:start;line-height:125%25;-inkscape-font-specification:Sans%20Bold%22%20x=%22268.929%22%20y=%22363.076%22%20font-size=%2218%22%20font-weight=%22700%22%20letter-spacing=%220%22%20word-spacing=%220%22%20font-family=%22Sans%22%20transform=%22translate(-196.643%20-263.29)%22%3E%3Ctspan%20x=%22268.929%22%20y=%22363.076%22%3EResult%3C/tspan%3E%3C/text%3E%3Ctext%20style=%22line-height:125%25;-inkscape-font-specification:Sans%20Bold;text-align:start%22%20x=%22323.214%22%20y=%22393.434%22%20font-size=%2218%22%20font-weight=%22700%22%20letter-spacing=%220%22%20word-spacing=%220%22%20font-family=%22Sans%22%20writing-mode=%22lr%22%20transform=%22translate(-196.643%20-263.29)%22%3E%3Ctspan%20x=%22323.214%22%20y=%22393.434%22%3EZ%3C/tspan%3E%3C/text%3E%3Ctext%20style=%22text-align:start;line-height:125%25;-inkscape-font-specification:Sans%20Bold%22%20x=%22252.857%22%20y=%22325.576%22%20font-size=%2218%22%20font-weight=%22700%22%20letter-spacing=%220%22%20word-spacing=%220%22%20font-family=%22Sans%22%20transform=%22translate(-196.643%20-263.29)%22%3E%3Ctspan%20x=%22252.857%22%20y=%22325.576%22%3Eop%3C/tspan%3E%3C/text%3E%3C/svg%3E"
  },
  "design": {
    "board": "icezum",
    "graph": {
      "blocks": [
        {
          "id": "ad1ce7dd-ef35-47ec-abfa-a4d3ffa0374f",
          "type": "basic.input",
          "data": {
            "name": "A",
            "range": "[31:0]",
            "pins": [
              {
                "index": "31",
                "name": "",
                "value": ""
              },
              {
                "index": "30",
                "name": "",
                "value": ""
              },
              {
                "index": "29",
                "name": "",
                "value": ""
              },
              {
                "index": "28",
                "name": "",
                "value": ""
              },
              {
                "index": "27",
                "name": "",
                "value": ""
              },
              {
                "index": "26",
                "name": "",
                "value": ""
              },
              {
                "index": "25",
                "name": "",
                "value": ""
              },
              {
                "index": "24",
                "name": "",
                "value": ""
              },
              {
                "index": "23",
                "name": "",
                "value": ""
              },
              {
                "index": "22",
                "name": "",
                "value": ""
              },
              {
                "index": "21",
                "name": "",
                "value": ""
              },
              {
                "index": "20",
                "name": "",
                "value": ""
              },
              {
                "index": "19",
                "name": "",
                "value": ""
              },
              {
                "index": "18",
                "name": "",
                "value": ""
              },
              {
                "index": "17",
                "name": "",
                "value": ""
              },
              {
                "index": "16",
                "name": "",
                "value": ""
              },
              {
                "index": "15",
                "name": "",
                "value": ""
              },
              {
                "index": "14",
                "name": "",
                "value": ""
              },
              {
                "index": "13",
                "name": "",
                "value": ""
              },
              {
                "index": "12",
                "name": "",
                "value": ""
              },
              {
                "index": "11",
                "name": "",
                "value": ""
              },
              {
                "index": "10",
                "name": "",
                "value": ""
              },
              {
                "index": "9",
                "name": "",
                "value": ""
              },
              {
                "index": "8",
                "name": "",
                "value": ""
              },
              {
                "index": "7",
                "name": "",
                "value": ""
              },
              {
                "index": "6",
                "name": "",
                "value": ""
              },
              {
                "index": "5",
                "name": "",
                "value": ""
              },
              {
                "index": "4",
                "name": "",
                "value": ""
              },
              {
                "index": "3",
                "name": "",
                "value": ""
              },
              {
                "index": "2",
                "name": "",
                "value": ""
              },
              {
                "index": "1",
                "name": "",
                "value": ""
              },
              {
                "index": "0",
                "name": "",
                "value": ""
              }
            ],
            "virtual": true,
            "clock": false
          },
          "position": {
            "x": -24,
            "y": -16
          }
        },
        {
          "id": "32553911-f024-4acc-b472-663a1287605d",
          "type": "basic.output",
          "data": {
            "name": "result",
            "range": "[31:0]",
            "pins": [
              {
                "index": "31",
                "name": "",
                "value": ""
              },
              {
                "index": "30",
                "name": "",
                "value": ""
              },
              {
                "index": "29",
                "name": "",
                "value": ""
              },
              {
                "index": "28",
                "name": "",
                "value": ""
              },
              {
                "index": "27",
                "name": "",
                "value": ""
              },
              {
                "index": "26",
                "name": "",
                "value": ""
              },
              {
                "index": "25",
                "name": "",
                "value": ""
              },
              {
                "index": "24",
                "name": "",
                "value": ""
              },
              {
                "index": "23",
                "name": "",
                "value": ""
              },
              {
                "index": "22",
                "name": "",
                "value": ""
              },
              {
                "index": "21",
                "name": "",
                "value": ""
              },
              {
                "index": "20",
                "name": "",
                "value": ""
              },
              {
                "index": "19",
                "name": "",
                "value": ""
              },
              {
                "index": "18",
                "name": "",
                "value": ""
              },
              {
                "index": "17",
                "name": "",
                "value": ""
              },
              {
                "index": "16",
                "name": "",
                "value": ""
              },
              {
                "index": "15",
                "name": "",
                "value": ""
              },
              {
                "index": "14",
                "name": "",
                "value": ""
              },
              {
                "index": "13",
                "name": "",
                "value": ""
              },
              {
                "index": "12",
                "name": "",
                "value": ""
              },
              {
                "index": "11",
                "name": "",
                "value": ""
              },
              {
                "index": "10",
                "name": "",
                "value": ""
              },
              {
                "index": "9",
                "name": "",
                "value": ""
              },
              {
                "index": "8",
                "name": "",
                "value": ""
              },
              {
                "index": "7",
                "name": "",
                "value": ""
              },
              {
                "index": "6",
                "name": "",
                "value": ""
              },
              {
                "index": "5",
                "name": "",
                "value": ""
              },
              {
                "index": "4",
                "name": "",
                "value": ""
              },
              {
                "index": "3",
                "name": "",
                "value": ""
              },
              {
                "index": "2",
                "name": "",
                "value": ""
              },
              {
                "index": "1",
                "name": "",
                "value": ""
              },
              {
                "index": "0",
                "name": "",
                "value": ""
              }
            ],
            "virtual": true
          },
          "position": {
            "x": 904,
            "y": 32
          }
        },
        {
          "id": "260818b1-365e-4d74-bc06-5a36f90c1f09",
          "type": "basic.input",
          "data": {
            "name": "B",
            "range": "[31:0]",
            "pins": [
              {
                "index": "31",
                "name": "",
                "value": ""
              },
              {
                "index": "30",
                "name": "",
                "value": ""
              },
              {
                "index": "29",
                "name": "",
                "value": ""
              },
              {
                "index": "28",
                "name": "",
                "value": ""
              },
              {
                "index": "27",
                "name": "",
                "value": ""
              },
              {
                "index": "26",
                "name": "",
                "value": ""
              },
              {
                "index": "25",
                "name": "",
                "value": ""
              },
              {
                "index": "24",
                "name": "",
                "value": ""
              },
              {
                "index": "23",
                "name": "",
                "value": ""
              },
              {
                "index": "22",
                "name": "",
                "value": ""
              },
              {
                "index": "21",
                "name": "",
                "value": ""
              },
              {
                "index": "20",
                "name": "",
                "value": ""
              },
              {
                "index": "19",
                "name": "",
                "value": ""
              },
              {
                "index": "18",
                "name": "",
                "value": ""
              },
              {
                "index": "17",
                "name": "",
                "value": ""
              },
              {
                "index": "16",
                "name": "",
                "value": ""
              },
              {
                "index": "15",
                "name": "",
                "value": ""
              },
              {
                "index": "14",
                "name": "",
                "value": ""
              },
              {
                "index": "13",
                "name": "",
                "value": ""
              },
              {
                "index": "12",
                "name": "",
                "value": ""
              },
              {
                "index": "11",
                "name": "",
                "value": ""
              },
              {
                "index": "10",
                "name": "",
                "value": ""
              },
              {
                "index": "9",
                "name": "",
                "value": ""
              },
              {
                "index": "8",
                "name": "",
                "value": ""
              },
              {
                "index": "7",
                "name": "",
                "value": ""
              },
              {
                "index": "6",
                "name": "",
                "value": ""
              },
              {
                "index": "5",
                "name": "",
                "value": ""
              },
              {
                "index": "4",
                "name": "",
                "value": ""
              },
              {
                "index": "3",
                "name": "",
                "value": ""
              },
              {
                "index": "2",
                "name": "",
                "value": ""
              },
              {
                "index": "1",
                "name": "",
                "value": ""
              },
              {
                "index": "0",
                "name": "",
                "value": ""
              }
            ],
            "virtual": true,
            "clock": false
          },
          "position": {
            "x": -24,
            "y": 184
          }
        },
        {
          "id": "d699853b-8d38-4bb2-999e-864200777f09",
          "type": "basic.output",
          "data": {
            "name": "Zero",
            "pins": [
              {
                "index": "0",
                "name": "",
                "value": ""
              }
            ],
            "virtual": true
          },
          "position": {
            "x": 912,
            "y": 328
          }
        },
        {
          "id": "f566d89c-7335-4c80-8013-62961d7ee805",
          "type": "basic.input",
          "data": {
            "name": "op",
            "range": "[3:0]",
            "pins": [
              {
                "index": "3",
                "name": "",
                "value": ""
              },
              {
                "index": "2",
                "name": "",
                "value": ""
              },
              {
                "index": "1",
                "name": "",
                "value": ""
              },
              {
                "index": "0",
                "name": "",
                "value": ""
              }
            ],
            "virtual": true,
            "clock": false
          },
          "position": {
            "x": -24,
            "y": 384
          }
        },
        {
          "id": "852b4ca9-13e4-42c2-acf0-935d7f1780a9",
          "type": "basic.code",
          "data": {
            "code": "always @*\n  case (op)\n     4'b0000 : result <= A & B;\n     4'b0001 : result <= A | B;\n     4'b0010 : result <= A + B;\n     4'b0110 : result <= A - B;\n     4'b0111 :\n        begin\n            if ( A < B )\n                result <= 1;\n            else\n                result <= 0;\n        end\n     4'b1100 : result <= ~(A | B);\n     default : result <= 0;\n  endcase\n  \nalways @*\n    if ( result == 0)\n        Zero = 1;\n    else\n        Zero = 0;",
            "params": [],
            "ports": {
              "in": [
                {
                  "name": "A",
                  "range": "[31:0]",
                  "size": 32
                },
                {
                  "name": "B",
                  "range": "[31:0]",
                  "size": 32
                },
                {
                  "name": "op",
                  "range": "[3:0]",
                  "size": 4
                }
              ],
              "out": [
                {
                  "name": "result",
                  "range": "[31:0]",
                  "size": 32
                },
                {
                  "name": "Zero"
                }
              ]
            }
          },
          "position": {
            "x": 152,
            "y": -88
          },
          "size": {
            "width": 656,
            "height": 600
          }
        }
      ],
      "wires": [
        {
          "source": {
            "block": "ad1ce7dd-ef35-47ec-abfa-a4d3ffa0374f",
            "port": "out"
          },
          "target": {
            "block": "852b4ca9-13e4-42c2-acf0-935d7f1780a9",
            "port": "A"
          },
          "size": 32
        },
        {
          "source": {
            "block": "260818b1-365e-4d74-bc06-5a36f90c1f09",
            "port": "out"
          },
          "target": {
            "block": "852b4ca9-13e4-42c2-acf0-935d7f1780a9",
            "port": "B"
          },
          "size": 32
        },
        {
          "source": {
            "block": "f566d89c-7335-4c80-8013-62961d7ee805",
            "port": "out"
          },
          "target": {
            "block": "852b4ca9-13e4-42c2-acf0-935d7f1780a9",
            "port": "op"
          },
          "size": 4
        },
        {
          "source": {
            "block": "852b4ca9-13e4-42c2-acf0-935d7f1780a9",
            "port": "result"
          },
          "target": {
            "block": "32553911-f024-4acc-b472-663a1287605d",
            "port": "in"
          },
          "size": 32
        },
        {
          "source": {
            "block": "852b4ca9-13e4-42c2-acf0-935d7f1780a9",
            "port": "Zero"
          },
          "target": {
            "block": "d699853b-8d38-4bb2-999e-864200777f09",
            "port": "in"
          }
        }
      ]
    }
  },
  "dependencies": {}
}