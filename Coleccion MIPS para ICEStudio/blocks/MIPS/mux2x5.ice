{
  "version": "1.2",
  "package": {
    "name": "",
    "version": "",
    "description": "",
    "author": "",
    "image": ""
  },
  "design": {
    "board": "icezum",
    "graph": {
      "blocks": [
        {
          "id": "3fcbb510-a950-4899-826a-6d273a29986b",
          "type": "basic.input",
          "data": {
            "name": "in0",
            "range": "[4:0]",
            "pins": [
              {
                "index": "4",
                "name": "",
                "value": ""
              },
              {
                "index": "3",
                "name": "",
                "value": ""
              },
              {
                "index": "2",
                "name": "",
                "value": ""
              },
              {
                "index": "1",
                "name": "",
                "value": ""
              },
              {
                "index": "0",
                "name": "",
                "value": ""
              }
            ],
            "virtual": true,
            "clock": false
          },
          "position": {
            "x": 136,
            "y": 128
          }
        },
        {
          "id": "83ac24fb-1cfa-4243-8ae6-2b95da065bc4",
          "type": "basic.input",
          "data": {
            "name": "in1",
            "range": "[4:0]",
            "pins": [
              {
                "index": "4",
                "name": "",
                "value": ""
              },
              {
                "index": "3",
                "name": "",
                "value": ""
              },
              {
                "index": "2",
                "name": "",
                "value": ""
              },
              {
                "index": "1",
                "name": "",
                "value": ""
              },
              {
                "index": "0",
                "name": "",
                "value": ""
              }
            ],
            "virtual": true,
            "clock": false
          },
          "position": {
            "x": 136,
            "y": 176
          }
        },
        {
          "id": "b4d2e8fe-2f82-4bb3-8693-0dd0ca16fc33",
          "type": "basic.output",
          "data": {
            "name": "out",
            "range": "[4:0]",
            "pins": [
              {
                "index": "4",
                "name": "",
                "value": ""
              },
              {
                "index": "3",
                "name": "",
                "value": ""
              },
              {
                "index": "2",
                "name": "",
                "value": ""
              },
              {
                "index": "1",
                "name": "",
                "value": ""
              },
              {
                "index": "0",
                "name": "",
                "value": ""
              }
            ],
            "virtual": true
          },
          "position": {
            "x": 696,
            "y": 176
          }
        },
        {
          "id": "acb02d6e-7817-427a-8353-849a0d16641d",
          "type": "basic.input",
          "data": {
            "name": "sel",
            "pins": [
              {
                "index": "0",
                "name": "",
                "value": ""
              }
            ],
            "virtual": true,
            "clock": false
          },
          "position": {
            "x": 136,
            "y": 224
          }
        },
        {
          "id": "55882adb-8f75-456d-9ed3-7b3e427e3eef",
          "type": "basic.code",
          "data": {
            "code": "always @*\n    if (sel == 0)\n        out = in0;\n    else\n        out = in1;",
            "params": [],
            "ports": {
              "in": [
                {
                  "name": "in0",
                  "range": "[4:0]",
                  "size": 5
                },
                {
                  "name": "in1",
                  "range": "[4:0]",
                  "size": 5
                },
                {
                  "name": "sel"
                }
              ],
              "out": [
                {
                  "name": "out",
                  "range": "[4:0]",
                  "size": 5
                }
              ]
            }
          },
          "position": {
            "x": 320,
            "y": 136
          },
          "size": {
            "width": 296,
            "height": 144
          }
        }
      ],
      "wires": [
        {
          "source": {
            "block": "acb02d6e-7817-427a-8353-849a0d16641d",
            "port": "out"
          },
          "target": {
            "block": "55882adb-8f75-456d-9ed3-7b3e427e3eef",
            "port": "sel"
          }
        },
        {
          "source": {
            "block": "3fcbb510-a950-4899-826a-6d273a29986b",
            "port": "out"
          },
          "target": {
            "block": "55882adb-8f75-456d-9ed3-7b3e427e3eef",
            "port": "in0"
          },
          "size": 5
        },
        {
          "source": {
            "block": "83ac24fb-1cfa-4243-8ae6-2b95da065bc4",
            "port": "out"
          },
          "target": {
            "block": "55882adb-8f75-456d-9ed3-7b3e427e3eef",
            "port": "in1"
          },
          "size": 5
        },
        {
          "source": {
            "block": "55882adb-8f75-456d-9ed3-7b3e427e3eef",
            "port": "out"
          },
          "target": {
            "block": "b4d2e8fe-2f82-4bb3-8693-0dd0ca16fc33",
            "port": "in"
          },
          "size": 5
        }
      ]
    }
  },
  "dependencies": {}
}