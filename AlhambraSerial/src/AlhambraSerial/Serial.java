/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AlhambraSerial;

import java.util.logging.Level;
import java.util.logging.Logger;
import jssc.SerialPort;
import jssc.SerialPortEvent;
import jssc.SerialPortEventListener;
import jssc.SerialPortException;
import jssc.SerialPortList;

/**
 *
 * @author corbera
 */
public class Serial {

    private static SerialPort serialPort;

    private DataModel myDataModel;
    private Configuration myConfig;
    private String[] portNames;
    private String port;
    private int baudRate;
    private int dataBits;
    private int stopBits;
    private int parity;

    public Serial(DataModel dm, Configuration conf) {
        myConfig = conf;
        portNames = SerialPortList.getPortNames();
        if (portNames.length > 0) {
            port = myConfig.getConfig("SerialPort", portNames[0]);
            if (findPort(port) < 0) {
                port = portNames[0];
            }
        } else {
            port = null;
        }
        baudRate = Integer.parseInt(myConfig.getConfig("BaudRate", "115200"));
        dataBits = Integer.parseInt(myConfig.getConfig("DataBits", "8"));
        stopBits = Integer.parseInt(myConfig.getConfig("StopBits", "1"));
        parity = Integer.parseInt(myConfig.getConfig("Parity", "0"));
        myDataModel = dm;
    }
    
    public int findPort(String p) {
        for (int i=0; i<portNames.length; i++) {
            if (portNames[i] == null ? p == null : portNames[i].equals(p))
                return i;
        }
        return -1;
    }

    public void setPort(String _port) {
        port = _port;
        myConfig.setConfig("SerialPort", port);
    }

    public void setBaudRate(int br) {
        baudRate = br;
        myConfig.setConfig("BaudRate", Integer.toString(br));
    }

    public void setDataBits(int db) {
        dataBits = db;
        myConfig.setConfig("DataBits", Integer.toString(db));
    }

    public void setStopBits(int sb) {
        stopBits = sb;
        myConfig.setConfig("StopBits", Integer.toString(sb));
    }

    public void setParity(int p) {
        parity = p;
        myConfig.setConfig("Parity", Integer.toString(p));
    }

    public String[] getPortNames() {
        return portNames;
    }

    public String getPort() {
        return port;
    }
    
    public int getBaudRate() {
        return baudRate;
    }

    public int getDataBits() {
        return dataBits;
    }

    public int getStopBits() {
        return stopBits;
    }

    public int getParity() {
        return parity;
    }

    public boolean connect() {
        serialPort = new SerialPort(port);
        try {
            serialPort.openPort();//Open port
            serialPort.setParams(baudRate, dataBits, stopBits, parity);//Set params
            int mask = SerialPort.MASK_RXCHAR + SerialPort.MASK_CTS + SerialPort.MASK_DSR;//Prepare mask
            serialPort.setEventsMask(mask);//Set mask
            serialPort.addEventListener(new SerialPortReader(myDataModel));//Add SerialPortEventListener
        } catch (SerialPortException ex) {
            System.out.println(ex);
            return false;
        }
        return true;
    }

    public void disconnect() {
        try {
            serialPort.removeEventListener();
            serialPort.closePort();
            serialPort = null;
        } catch (SerialPortException ex) {
            Logger.getLogger(Serial.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /*
 * In this class must implement the method serialEvent, through it we learn about 
 * events that happened to our port. But we will not report on all events but only 
 * those that we put in the mask. In this case the arrival of the data and change the 
 * status lines CTS and DSR
     */
    static class SerialPortReader implements SerialPortEventListener {

        private DataModel myDataModel;

        public SerialPortReader(DataModel dm) {
            myDataModel = dm;
        }

        @Override
        public void serialEvent(SerialPortEvent event) {
            if (event.isRXCHAR()) {//If data is available
                //System.out.println("serial.AlhambraSerial.SerialPortReader.serialEvent()");
                try {
                    byte buffer[] = serialPort.readBytes(1);
                    myDataModel.putData(buffer[0]);
                    //System.err.print(buffer[0] + " ");
                    //putData(buffer[0]);
                } catch (SerialPortException ex) {
                    System.out.println(ex);
                }
            } else if (event.isCTS()) {//If CTS line has changed state
                if (event.getEventValue() == 1) {//If line is ON
                    System.out.println("CTS - ON");
                } else {
                    System.out.println("CTS - OFF");
                }
            } else if (event.isDSR()) {///If DSR line has changed state
                if (event.getEventValue() == 1) {//If line is ON
                    System.out.println("DSR - ON");
                } else {
                    System.out.println("DSR - OFF");
                }
            }
        }
    }
}
