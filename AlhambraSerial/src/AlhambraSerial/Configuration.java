/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AlhambraSerial;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Properties;

/**
 *
 * @author corbera
 */
public class Configuration {

    private Properties prop = null;
    private String fileName = "config.txt";
    
    public Configuration() {
        prop = new Properties();
        InputStream is = null;

        try {
            is = new FileInputStream(fileName);
            prop.load(is);
        } catch (IOException e) {
            System.out.println(e.toString());
        }
    }
    
    public String getConfig(String key, String def) {
        return prop.getProperty(key, def);
    }
    
    public void setConfig(String key, String val) {
        prop.setProperty(key, val);
    }
    
    public void saveConfiguration() {
        OutputStream os = null;

        try {
            os = new FileOutputStream(fileName);
            prop.store(os, "");
        } catch (IOException e) {
            System.out.println(e.toString());
        }        
    }
}
