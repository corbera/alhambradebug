/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AlhambraSerial;

import java.util.LinkedList;
import java.util.List;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author corbera
 */
public class DataModel {

    enum format {
        BIN, HEX, INT, UINT
    };
    String formatStr[] = {"Bin", "Hex", "Int", "Uint", "MIPS asm"};
    String colFormat[];

    public DefaultTableModel myTableModel;
    private JTable myJTable;
    private Configuration myConfig;
    private static final int NW = 5;
    private static final int NB = 3;
    private List<int[]> statesList;
    private String columnNames[];
    private byte buffer[];
    int state = 0;
    int maxState;
    int row = 0;
    int cols;

    MIPSAsmFormat mipsFormat;

    public DataModel(JTable table, Configuration conf) {
        myJTable = table;

        myConfig = conf;

        cols = NW + NB + 8;
        statesList = new LinkedList<int[]>();
        columnNames = new String[cols];
        for (int i = 0; i < NW; i++) {
            columnNames[i] = new String("W" + i);
        }
        for (int i = 0; i < NB; i++) {
            columnNames[NW + i] = new String("B" + i);
        }
        for (int i = 0; i < 8; i++) {
            columnNames[NW + NB + i] = new String("b" + i);
        }

        colFormat = new String[cols];
        for (int i = 0; i < cols; i++) {
            colFormat[i] = myConfig.getConfig("Col" + i, "Hex");
        }

        myTableModel = new DefaultTableModel(columnNames, 0);
        maxState = NW * 4 + NB + 1;
        buffer = new byte[maxState];

        mipsFormat = new MIPSAsmFormat();
    }

    public void putData(byte d) {
        buffer[state] = d;
        state++;
        System.err.print(d + " ");
        if (state == maxState) {
            newState();
            state = 0;
            System.err.println();
        }
    }

    private void newState() {
        int[] states = new int[cols];
        int off;
        for (int i = 0; i < NW; i++) {
            off = 4 * i;
            states[i] = (int) (buffer[off] << 24) | (int) (buffer[off + 1] << 16) | (int) (buffer[off + 2] << 8) | (int) (buffer[off + 3]);
        }
        off = 4 * NW;
        for (int i = 0; i < NB; i++) {
            states[NW + i] = buffer[off + i];
        }
        off = 4 * NW + NB;
        for (int i = 0; i < 8; i++) {
            states[NW + NB + i] = (buffer[off] >> i) & 1;
        }
        statesList.add(states);
        //myTableModel.insertRow(0, stateToStrArray(states));
        myTableModel.insertRow(row++, stateToStrArray(states));
        //myJTable.changeSelection(myJTable.getRowCount(), 0, false, false);
    }

    String[] stateToStrArray(int states[]) {
        String[] str = new String[cols];
        for (int i = 0; i < cols; i++) {
            if (i < NW) {  // word of 32 bits
                switch (colFormat[i]) {
                    case "Hex":
                        str[i] = String.format("%08X", states[i]);
                        break;
                    case "Bin":
                        str[i] = String.format("%32s", Integer.toBinaryString(states[i])).replace(" ", "0");
                        break;
                    case "Int":
                        str[i] = Integer.toString(states[i]);
                        break;
                    case "Uint":
                        str[i] = Long.toString(states[i] & 0xFFFFFFFFL);
                        break;
                    case "MIPS asm":
                        //states[i] = 0b00000000001000110000000000001000;
                        str[i] = mipsFormat.decode(states[i]);
                        break;
                }
            } else if (i < NW + NB) {  // word 8 bits
                switch (colFormat[i]) {
                    case "Hex":
                        str[i] = String.format("%02X", (byte) states[i]);
                        break;
                    case "Bin":
                        str[i] = String.format("%32s", Integer.toBinaryString((byte) states[i])).replace(" ", "0");
                        str[i] = str[i].substring(str[i].length() - 8, str[i].length());
                        break;
                    case "Int":
                        str[i] = Integer.toString((byte) states[i]);
                        break;
                    case "Uint":
                        str[i] = String.format("%d", states[i] & 0xFF);
                        break;
                    case "MIPS asm":
                        str[i] = String.format("%02X", (byte) states[i]);
                        break;
                }
            } else {  // 8 bit packet in one byte
                str[i] = Integer.toString(states[i]);
            }
        }
        return str;
    }

    public void clearData() {
        System.out.println("Clear");
        myTableModel.setRowCount(0);
        statesList.clear();
        row = 0;
    }

    public void setColumnFormat(int col, String f) {
        colFormat[col] = f;
        myConfig.setConfig("Col" + col, f);
    }

    public String getColumnFormat(int col) {
        return colFormat[col];
    }

    public String[] getFormatStr() {
        return formatStr;
    }

    public void redraw() {
        row = 0;
        myTableModel.setRowCount(0);
        for (int[] s : statesList) {
            myTableModel.insertRow(row++, stateToStrArray(s));
        }
    }
}
