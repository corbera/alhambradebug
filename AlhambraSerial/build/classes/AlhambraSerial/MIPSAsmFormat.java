/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package AlhambraSerial;

/**
 *
 * @author casa
 */
public class MIPSAsmFormat {

    String instructions[] = {
        "add", "Add", "R", "0x00", "0x20",
        "addi", "Add Immediate", "I", "0x08", "NA",
        "addiu", "Add Unsigned Immediate", "I", "0x09", "NA",
        "addu", "Add Unsigned", "R", "0x00", "0x21",
        "and", "Bitwise AND", "R", "0x00", "0x24",
        "andi", "Bitwise AND Immediate", "I", "0x0C", "NA",
        "beq", "Branch if Equal", "I", "0x04", "NA",
        "blez", "Branch if Less Than or Equal to Zero", "I", "0x06", "NA",
        "bne", "Branch if Not Equal", "I", "0x05", "NA",
        "bgtz", "Branch on Greater Than Zero", "I", "0x07", "NA",
        "div", "Divide", "R", "0x00", "0x1A",
        "divu", "Unsigned Divide", "R", "0x00", "0x1B",
        "j", "Jump to Address", "J", "0x02", "NA",
        "jal", "Jump and Link", "J", "0x03", "NA",
        "jr", "Jump to Address in Register", "R", "0x00", "0x08",
        "lb", "Load Byte", "I", "0x20", "NA",
        "lbu", "Load Byte Unsigned", "I", "0x24", "NA",
        "lhu", "Load Halfword Unsigned", "I", "0x25", "NA",
        "lui", "Load Upper Immediate", "I", "0x0F", "NA",
        "lw", "Load Word", "I", "0x23", "NA",
        "mfhi", "Move from HI Register", "R", "0x00", "0x10",
        "mthi", "Move to HI Register", "R", "0x00", "0x11",
        "mflo", "Move from LO Register", "R", "0x00", "0x12",
        "mtlo", "Move to LO Register", "R", "0x00", "0x13",
        "mfc0", "Move from Coprocessor 0", "R", "0x10", "NA",
        "mult", "Multiply", "R", "0x00", "0x18",
        "multu", "Unsigned Multiply", "R", "0x00", "0x19",
        "nor", "Bitwise NOR (NOT-OR)", "R", "0x00", "0x27",
        "xor", "Bitwise XOR (Exclusive-OR)", "R", "0x00", "0x26",
        "or", "Bitwise OR", "R", "0x00", "0x25",
        "ori", "Bitwise OR Immediate", "I", "0x0D", "NA",
        "sb", "Store Byte", "I", "0x28", "NA",
        "sh", "Store Halfword", "I", "0x29", "NA",
        "slt", "Set to 1 if Less Than", "R", "0x00", "0x2A",
        "slti", "Set to 1 if Less Than Immediate", "I", "0x0A", "NA",
        "sltiu", "Set to 1 if Less Than Unsigned Immediate", "I", "0x0B", "NA",
        "sltu", "Set to 1 if Less Than Unsigned", "R", "0x00", "0x2B",
        "sll", "Logical Shift Left", "R", "0x00", "0x00",
        "srl", "Logical Shift Right (0-extended)", "R", "0x00", "0x02",
        "sra", "Arithmetic Shift Right (sign-extended)", "R", "0x00", "0x03",
        "sub", "Subtract", "R", "0x00", "0x22",
        "subu", "Unsigned Subtract", "R", "0x00", "0x23",
        "sw", "Store Word", "I", "0x2B", "NA"};

    public String decode(int inst) {
        if (inst == 0)
            return "nop";
        int opcode = (inst >> 26) & 0x3F;
        int pos = getPos(opcode);
        if (pos < 0) {
            return "UNK";
        }
        String format = getFormat(pos);
        switch (format) {
            case "R": {
                int funct = inst & 0x3F;
                pos = getPos(opcode, funct);
                if (pos < 0) {
                    return "UNK";
                }
                String rs = "$" + ((inst >> 21) & 0x1F);
                String rt = "$" + ((inst >> 16) & 0x1F);
                String rd = "$" + ((inst >> 11) & 0x1F);
                String name = getName(pos);
                switch (name) {
                    case "jr":
                        return name + " " + rs;
                    default:
                        return name + " " + rd + ", " + rs + ", " + rt;
                }
            }
            case "I": {
                String inm = String.format("%04X", inst & 0xFFFF);
                String rs = "$" + ((inst >> 21) & 0x1F);
                String rt = "$" + ((inst >> 16) & 0x1F);
                String name = getName(pos);
                switch (name) {
                    case "lb":
                    case "lbu":
                    case "lhu":
                    case "lw":
                    case "sb":
                    case "sh":
                    case "sw":
                        return name + " " + rt + ", 0x" + inm + "(" + rs + ")";
                    case "lui":
                        return name + " " + rt + ", 0x" + inm;
                    case "beq":
                    case "bne":
                        return name + " " + rs + ", " + rt + ", 0x" + inm;                        
                    case "bgtz":
                    case "blez":
                        return name + " " + rs + ", 0x" + inm;                        
                    default:
                        return name + " " + rt + ", " + rs + ", 0x" + inm;
                }
            }
            case "J": {
                String dir = String.format("%08X", inst & 0x3FFFFFF);
                return getName(pos) + " 0x" + dir;
            }
        }
        return "UNK";
    }

    private int getPos(int opcode) {
        String opcodeS = "0x" + String.format("%02X", (byte) opcode);
        for (int i = 0; i < instructions.length; i++) {
            if (opcodeS.equals(instructions[i])) {
                return i;
            }
        }
        return -1;
    }

    private int getPos(int opcode, int funct) {
        String opcodeS = "0x" + String.format("%02X", (byte) opcode);
        String functS = "0x" + String.format("%02X", (byte) funct);
        for (int i = 0; i < instructions.length; i++) {
            if (opcodeS.equals(instructions[i]) && functS.equals(instructions[i + 1])) {
                return i;
            }
        }
        return -1;
    }

    private String getFormat(int pos) {
        return instructions[pos - 1];
    }

    private String getName(int pos) {
        return instructions[pos - 3];
    }
}
