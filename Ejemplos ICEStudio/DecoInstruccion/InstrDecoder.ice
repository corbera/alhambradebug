{
  "version": "1.2",
  "package": {
    "name": "",
    "version": "",
    "description": "",
    "author": "",
    "image": ""
  },
  "design": {
    "board": "icezum",
    "graph": {
      "blocks": [
        {
          "id": "eb9e801d-2f3a-4240-8283-875a8026bbea",
          "type": "basic.output",
          "data": {
            "name": "ib31_26",
            "range": "[5:0]",
            "pins": [
              {
                "index": "5",
                "name": "",
                "value": ""
              },
              {
                "index": "4",
                "name": "",
                "value": ""
              },
              {
                "index": "3",
                "name": "",
                "value": ""
              },
              {
                "index": "2",
                "name": "",
                "value": ""
              },
              {
                "index": "1",
                "name": "",
                "value": ""
              },
              {
                "index": "0",
                "name": "",
                "value": ""
              }
            ],
            "virtual": true
          },
          "position": {
            "x": 792,
            "y": 120
          }
        },
        {
          "id": "7248d5a6-42f2-4ce5-82b2-36862479335f",
          "type": "basic.output",
          "data": {
            "name": "ib25_21",
            "range": "[4:0]",
            "pins": [
              {
                "index": "4",
                "name": "",
                "value": ""
              },
              {
                "index": "3",
                "name": "",
                "value": ""
              },
              {
                "index": "2",
                "name": "",
                "value": ""
              },
              {
                "index": "1",
                "name": "",
                "value": ""
              },
              {
                "index": "0",
                "name": "",
                "value": ""
              }
            ],
            "virtual": true
          },
          "position": {
            "x": 792,
            "y": 176
          }
        },
        {
          "id": "389162d2-3f67-48b1-8682-56fdd84a3de4",
          "type": "basic.output",
          "data": {
            "name": "ib20_16",
            "range": "[4:0]",
            "pins": [
              {
                "index": "4",
                "name": "",
                "value": ""
              },
              {
                "index": "3",
                "name": "",
                "value": ""
              },
              {
                "index": "2",
                "name": "",
                "value": ""
              },
              {
                "index": "1",
                "name": "",
                "value": ""
              },
              {
                "index": "0",
                "name": "",
                "value": ""
              }
            ],
            "virtual": true
          },
          "position": {
            "x": 792,
            "y": 224
          }
        },
        {
          "id": "373c74a5-3509-4225-a3e0-e94f7e625957",
          "type": "basic.input",
          "data": {
            "name": "ib",
            "range": "[31:0]",
            "pins": [
              {
                "index": "31",
                "name": "",
                "value": ""
              },
              {
                "index": "30",
                "name": "",
                "value": ""
              },
              {
                "index": "29",
                "name": "",
                "value": ""
              },
              {
                "index": "28",
                "name": "",
                "value": ""
              },
              {
                "index": "27",
                "name": "",
                "value": ""
              },
              {
                "index": "26",
                "name": "",
                "value": ""
              },
              {
                "index": "25",
                "name": "",
                "value": ""
              },
              {
                "index": "24",
                "name": "",
                "value": ""
              },
              {
                "index": "23",
                "name": "",
                "value": ""
              },
              {
                "index": "22",
                "name": "",
                "value": ""
              },
              {
                "index": "21",
                "name": "",
                "value": ""
              },
              {
                "index": "20",
                "name": "",
                "value": ""
              },
              {
                "index": "19",
                "name": "",
                "value": ""
              },
              {
                "index": "18",
                "name": "",
                "value": ""
              },
              {
                "index": "17",
                "name": "",
                "value": ""
              },
              {
                "index": "16",
                "name": "",
                "value": ""
              },
              {
                "index": "15",
                "name": "",
                "value": ""
              },
              {
                "index": "14",
                "name": "",
                "value": ""
              },
              {
                "index": "13",
                "name": "",
                "value": ""
              },
              {
                "index": "12",
                "name": "",
                "value": ""
              },
              {
                "index": "11",
                "name": "",
                "value": ""
              },
              {
                "index": "10",
                "name": "",
                "value": ""
              },
              {
                "index": "9",
                "name": "",
                "value": ""
              },
              {
                "index": "8",
                "name": "",
                "value": ""
              },
              {
                "index": "7",
                "name": "",
                "value": ""
              },
              {
                "index": "6",
                "name": "",
                "value": ""
              },
              {
                "index": "5",
                "name": "",
                "value": ""
              },
              {
                "index": "4",
                "name": "",
                "value": ""
              },
              {
                "index": "3",
                "name": "",
                "value": ""
              },
              {
                "index": "2",
                "name": "",
                "value": ""
              },
              {
                "index": "1",
                "name": "",
                "value": ""
              },
              {
                "index": "0",
                "name": "",
                "value": ""
              }
            ],
            "virtual": true,
            "clock": false
          },
          "position": {
            "x": 216,
            "y": 280
          }
        },
        {
          "id": "1cbc8463-75ce-4efd-94ac-8ed96276e414",
          "type": "basic.output",
          "data": {
            "name": "ib15_11",
            "range": "[4:0]",
            "pins": [
              {
                "index": "4",
                "name": "",
                "value": ""
              },
              {
                "index": "3",
                "name": "",
                "value": ""
              },
              {
                "index": "2",
                "name": "",
                "value": ""
              },
              {
                "index": "1",
                "name": "",
                "value": ""
              },
              {
                "index": "0",
                "name": "",
                "value": ""
              }
            ],
            "virtual": true
          },
          "position": {
            "x": 792,
            "y": 280
          }
        },
        {
          "id": "a7f0093a-3e7f-44b6-8474-e47f1a71aae1",
          "type": "basic.output",
          "data": {
            "name": "ib5_0",
            "range": "[5:0]",
            "pins": [
              {
                "index": "5",
                "name": "",
                "value": ""
              },
              {
                "index": "4",
                "name": "",
                "value": ""
              },
              {
                "index": "3",
                "name": "",
                "value": ""
              },
              {
                "index": "2",
                "name": "",
                "value": ""
              },
              {
                "index": "1",
                "name": "",
                "value": ""
              },
              {
                "index": "0",
                "name": "",
                "value": ""
              }
            ],
            "virtual": true
          },
          "position": {
            "x": 792,
            "y": 328
          }
        },
        {
          "id": "cc4cd634-6f8e-4974-bdda-79e5092fd249",
          "type": "basic.output",
          "data": {
            "name": "ib15_0",
            "range": "[15:0]",
            "pins": [
              {
                "index": "15",
                "name": "",
                "value": ""
              },
              {
                "index": "14",
                "name": "",
                "value": ""
              },
              {
                "index": "13",
                "name": "",
                "value": ""
              },
              {
                "index": "12",
                "name": "",
                "value": ""
              },
              {
                "index": "11",
                "name": "",
                "value": ""
              },
              {
                "index": "10",
                "name": "",
                "value": ""
              },
              {
                "index": "9",
                "name": "",
                "value": ""
              },
              {
                "index": "8",
                "name": "",
                "value": ""
              },
              {
                "index": "7",
                "name": "",
                "value": ""
              },
              {
                "index": "6",
                "name": "",
                "value": ""
              },
              {
                "index": "5",
                "name": "",
                "value": ""
              },
              {
                "index": "4",
                "name": "",
                "value": ""
              },
              {
                "index": "3",
                "name": "",
                "value": ""
              },
              {
                "index": "2",
                "name": "",
                "value": ""
              },
              {
                "index": "1",
                "name": "",
                "value": ""
              },
              {
                "index": "0",
                "name": "",
                "value": ""
              }
            ],
            "virtual": true
          },
          "position": {
            "x": 792,
            "y": 376
          }
        },
        {
          "id": "59197c57-0f30-47b2-bb3d-b49bcbce5207",
          "type": "basic.output",
          "data": {
            "name": "ib25_0",
            "range": "[25:0]",
            "pins": [
              {
                "index": "25",
                "name": "",
                "value": ""
              },
              {
                "index": "24",
                "name": "",
                "value": ""
              },
              {
                "index": "23",
                "name": "",
                "value": ""
              },
              {
                "index": "22",
                "name": "",
                "value": ""
              },
              {
                "index": "21",
                "name": "",
                "value": ""
              },
              {
                "index": "20",
                "name": "",
                "value": ""
              },
              {
                "index": "19",
                "name": "",
                "value": ""
              },
              {
                "index": "18",
                "name": "",
                "value": ""
              },
              {
                "index": "17",
                "name": "",
                "value": ""
              },
              {
                "index": "16",
                "name": "",
                "value": ""
              },
              {
                "index": "15",
                "name": "",
                "value": ""
              },
              {
                "index": "14",
                "name": "",
                "value": ""
              },
              {
                "index": "13",
                "name": "",
                "value": ""
              },
              {
                "index": "12",
                "name": "",
                "value": ""
              },
              {
                "index": "11",
                "name": "",
                "value": ""
              },
              {
                "index": "10",
                "name": "",
                "value": ""
              },
              {
                "index": "9",
                "name": "",
                "value": ""
              },
              {
                "index": "8",
                "name": "",
                "value": ""
              },
              {
                "index": "7",
                "name": "",
                "value": ""
              },
              {
                "index": "6",
                "name": "",
                "value": ""
              },
              {
                "index": "5",
                "name": "",
                "value": ""
              },
              {
                "index": "4",
                "name": "",
                "value": ""
              },
              {
                "index": "3",
                "name": "",
                "value": ""
              },
              {
                "index": "2",
                "name": "",
                "value": ""
              },
              {
                "index": "1",
                "name": "",
                "value": ""
              },
              {
                "index": "0",
                "name": "",
                "value": ""
              }
            ],
            "virtual": true
          },
          "position": {
            "x": 792,
            "y": 432
          }
        },
        {
          "id": "02a024d7-d1fc-433f-9e5f-25fb264f329c",
          "type": "basic.code",
          "data": {
            "code": "assign ib31_26 = inst[31:26];\nassign ib25_21 = inst[25:21];\nassign ib20_16 = inst[20:16];\nassign ib15_11 = inst[15:11];\nassign ib5_0 = inst[5:0];\nassign ib15_0 = inst[15:0];\nassign ib25_0 = inst[25:0];",
            "params": [],
            "ports": {
              "in": [
                {
                  "name": "inst",
                  "range": "[31:0]",
                  "size": 32
                }
              ],
              "out": [
                {
                  "name": "ib31_26",
                  "range": "[5:0]",
                  "size": 6
                },
                {
                  "name": "ib25_21",
                  "range": "[4:0]",
                  "size": 5
                },
                {
                  "name": "ib20_16",
                  "range": "[4:0]",
                  "size": 5
                },
                {
                  "name": "ib15_11",
                  "range": "[4:0]",
                  "size": 5
                },
                {
                  "name": "ib5_0",
                  "range": "[5:0]",
                  "size": 6
                },
                {
                  "name": "ib15_0",
                  "range": "[15:0]",
                  "size": 16
                },
                {
                  "name": "ib25_0",
                  "range": "[25:0]",
                  "size": 26
                }
              ]
            }
          },
          "position": {
            "x": 416,
            "y": 128
          },
          "size": {
            "width": 280,
            "height": 360
          }
        }
      ],
      "wires": [
        {
          "source": {
            "block": "373c74a5-3509-4225-a3e0-e94f7e625957",
            "port": "out"
          },
          "target": {
            "block": "02a024d7-d1fc-433f-9e5f-25fb264f329c",
            "port": "inst"
          },
          "size": 32
        },
        {
          "source": {
            "block": "02a024d7-d1fc-433f-9e5f-25fb264f329c",
            "port": "ib31_26"
          },
          "target": {
            "block": "eb9e801d-2f3a-4240-8283-875a8026bbea",
            "port": "in"
          },
          "size": 6
        },
        {
          "source": {
            "block": "02a024d7-d1fc-433f-9e5f-25fb264f329c",
            "port": "ib25_21"
          },
          "target": {
            "block": "7248d5a6-42f2-4ce5-82b2-36862479335f",
            "port": "in"
          },
          "size": 5
        },
        {
          "source": {
            "block": "02a024d7-d1fc-433f-9e5f-25fb264f329c",
            "port": "ib20_16"
          },
          "target": {
            "block": "389162d2-3f67-48b1-8682-56fdd84a3de4",
            "port": "in"
          },
          "size": 5
        },
        {
          "source": {
            "block": "02a024d7-d1fc-433f-9e5f-25fb264f329c",
            "port": "ib15_11"
          },
          "target": {
            "block": "1cbc8463-75ce-4efd-94ac-8ed96276e414",
            "port": "in"
          },
          "size": 5
        },
        {
          "source": {
            "block": "02a024d7-d1fc-433f-9e5f-25fb264f329c",
            "port": "ib5_0"
          },
          "target": {
            "block": "a7f0093a-3e7f-44b6-8474-e47f1a71aae1",
            "port": "in"
          },
          "size": 6
        },
        {
          "source": {
            "block": "02a024d7-d1fc-433f-9e5f-25fb264f329c",
            "port": "ib15_0"
          },
          "target": {
            "block": "cc4cd634-6f8e-4974-bdda-79e5092fd249",
            "port": "in"
          },
          "size": 16
        },
        {
          "source": {
            "block": "02a024d7-d1fc-433f-9e5f-25fb264f329c",
            "port": "ib25_0"
          },
          "target": {
            "block": "59197c57-0f30-47b2-bb3d-b49bcbce5207",
            "port": "in"
          },
          "size": 26
        }
      ]
    }
  },
  "dependencies": {}
}