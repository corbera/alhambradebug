{
  "version": "1.2",
  "package": {
    "name": "",
    "version": "",
    "description": "",
    "author": "",
    "image": ""
  },
  "design": {
    "board": "icezum",
    "graph": {
      "blocks": [
        {
          "id": "43f394b7-622c-48fb-9456-31cd6a6c5953",
          "type": "basic.input",
          "data": {
            "name": "clk",
            "pins": [
              {
                "index": "0",
                "name": "",
                "value": ""
              }
            ],
            "virtual": true,
            "clock": true
          },
          "position": {
            "x": 200,
            "y": 160
          }
        },
        {
          "id": "0864bed1-5b93-4460-84d3-1d72f9e02ef6",
          "type": "basic.input",
          "data": {
            "name": "sync",
            "pins": [
              {
                "index": "0",
                "name": "",
                "value": ""
              }
            ],
            "virtual": true,
            "clock": false
          },
          "position": {
            "x": 200,
            "y": 216
          }
        },
        {
          "id": "f6338ffc-3359-4dc2-b36c-727c79a0457a",
          "type": "basic.output",
          "data": {
            "name": "a",
            "range": "[31:0]",
            "pins": [
              {
                "index": "31",
                "name": "",
                "value": ""
              },
              {
                "index": "30",
                "name": "",
                "value": ""
              },
              {
                "index": "29",
                "name": "",
                "value": ""
              },
              {
                "index": "28",
                "name": "",
                "value": ""
              },
              {
                "index": "27",
                "name": "",
                "value": ""
              },
              {
                "index": "26",
                "name": "",
                "value": ""
              },
              {
                "index": "25",
                "name": "",
                "value": ""
              },
              {
                "index": "24",
                "name": "",
                "value": ""
              },
              {
                "index": "23",
                "name": "",
                "value": ""
              },
              {
                "index": "22",
                "name": "",
                "value": ""
              },
              {
                "index": "21",
                "name": "",
                "value": ""
              },
              {
                "index": "20",
                "name": "",
                "value": ""
              },
              {
                "index": "19",
                "name": "",
                "value": ""
              },
              {
                "index": "18",
                "name": "",
                "value": ""
              },
              {
                "index": "17",
                "name": "",
                "value": ""
              },
              {
                "index": "16",
                "name": "",
                "value": ""
              },
              {
                "index": "15",
                "name": "",
                "value": ""
              },
              {
                "index": "14",
                "name": "",
                "value": ""
              },
              {
                "index": "13",
                "name": "",
                "value": ""
              },
              {
                "index": "12",
                "name": "",
                "value": ""
              },
              {
                "index": "11",
                "name": "",
                "value": ""
              },
              {
                "index": "10",
                "name": "",
                "value": ""
              },
              {
                "index": "9",
                "name": "",
                "value": ""
              },
              {
                "index": "8",
                "name": "",
                "value": ""
              },
              {
                "index": "7",
                "name": "",
                "value": ""
              },
              {
                "index": "6",
                "name": "",
                "value": ""
              },
              {
                "index": "5",
                "name": "",
                "value": ""
              },
              {
                "index": "4",
                "name": "",
                "value": ""
              },
              {
                "index": "3",
                "name": "",
                "value": ""
              },
              {
                "index": "2",
                "name": "",
                "value": ""
              },
              {
                "index": "1",
                "name": "",
                "value": ""
              },
              {
                "index": "0",
                "name": "",
                "value": ""
              }
            ],
            "virtual": true
          },
          "position": {
            "x": 736,
            "y": 232
          }
        },
        {
          "id": "47d8b235-fcc5-40f0-af14-17dfae40b975",
          "type": "basic.input",
          "data": {
            "name": "write",
            "pins": [
              {
                "index": "0",
                "name": "",
                "value": ""
              }
            ],
            "virtual": true,
            "clock": false
          },
          "position": {
            "x": 200,
            "y": 264
          }
        },
        {
          "id": "ab957d18-156a-4ae5-a1c5-046f0d1663e1",
          "type": "basic.input",
          "data": {
            "name": "da",
            "range": "[4:0]",
            "pins": [
              {
                "index": "4",
                "name": "",
                "value": ""
              },
              {
                "index": "3",
                "name": "",
                "value": ""
              },
              {
                "index": "2",
                "name": "",
                "value": ""
              },
              {
                "index": "1",
                "name": "",
                "value": ""
              },
              {
                "index": "0",
                "name": "",
                "value": ""
              }
            ],
            "virtual": true,
            "clock": false
          },
          "position": {
            "x": 200,
            "y": 320
          }
        },
        {
          "id": "9adff702-e95d-4e15-94c5-d36cd38bb6ea",
          "type": "basic.input",
          "data": {
            "name": "db",
            "range": "[4:0]",
            "pins": [
              {
                "index": "4",
                "name": "",
                "value": ""
              },
              {
                "index": "3",
                "name": "",
                "value": ""
              },
              {
                "index": "2",
                "name": "",
                "value": ""
              },
              {
                "index": "1",
                "name": "",
                "value": ""
              },
              {
                "index": "0",
                "name": "",
                "value": ""
              }
            ],
            "virtual": true,
            "clock": false
          },
          "position": {
            "x": 200,
            "y": 376
          }
        },
        {
          "id": "4723d5cc-53f3-4ca3-8ff4-863ee4d017ba",
          "type": "basic.output",
          "data": {
            "name": "b",
            "range": "[31:0]",
            "pins": [
              {
                "index": "31",
                "name": "",
                "value": ""
              },
              {
                "index": "30",
                "name": "",
                "value": ""
              },
              {
                "index": "29",
                "name": "",
                "value": ""
              },
              {
                "index": "28",
                "name": "",
                "value": ""
              },
              {
                "index": "27",
                "name": "",
                "value": ""
              },
              {
                "index": "26",
                "name": "",
                "value": ""
              },
              {
                "index": "25",
                "name": "",
                "value": ""
              },
              {
                "index": "24",
                "name": "",
                "value": ""
              },
              {
                "index": "23",
                "name": "",
                "value": ""
              },
              {
                "index": "22",
                "name": "",
                "value": ""
              },
              {
                "index": "21",
                "name": "",
                "value": ""
              },
              {
                "index": "20",
                "name": "",
                "value": ""
              },
              {
                "index": "19",
                "name": "",
                "value": ""
              },
              {
                "index": "18",
                "name": "",
                "value": ""
              },
              {
                "index": "17",
                "name": "",
                "value": ""
              },
              {
                "index": "16",
                "name": "",
                "value": ""
              },
              {
                "index": "15",
                "name": "",
                "value": ""
              },
              {
                "index": "14",
                "name": "",
                "value": ""
              },
              {
                "index": "13",
                "name": "",
                "value": ""
              },
              {
                "index": "12",
                "name": "",
                "value": ""
              },
              {
                "index": "11",
                "name": "",
                "value": ""
              },
              {
                "index": "10",
                "name": "",
                "value": ""
              },
              {
                "index": "9",
                "name": "",
                "value": ""
              },
              {
                "index": "8",
                "name": "",
                "value": ""
              },
              {
                "index": "7",
                "name": "",
                "value": ""
              },
              {
                "index": "6",
                "name": "",
                "value": ""
              },
              {
                "index": "5",
                "name": "",
                "value": ""
              },
              {
                "index": "4",
                "name": "",
                "value": ""
              },
              {
                "index": "3",
                "name": "",
                "value": ""
              },
              {
                "index": "2",
                "name": "",
                "value": ""
              },
              {
                "index": "1",
                "name": "",
                "value": ""
              },
              {
                "index": "0",
                "name": "",
                "value": ""
              }
            ],
            "virtual": true
          },
          "position": {
            "x": 736,
            "y": 416
          }
        },
        {
          "id": "fba41b7e-6e66-4324-8b24-2cd4e2c4189a",
          "type": "basic.input",
          "data": {
            "name": "dc",
            "range": "[4:0]",
            "pins": [
              {
                "index": "4",
                "name": "",
                "value": ""
              },
              {
                "index": "3",
                "name": "",
                "value": ""
              },
              {
                "index": "2",
                "name": "",
                "value": ""
              },
              {
                "index": "1",
                "name": "",
                "value": ""
              },
              {
                "index": "0",
                "name": "",
                "value": ""
              }
            ],
            "virtual": true,
            "clock": false
          },
          "position": {
            "x": 200,
            "y": 424
          }
        },
        {
          "id": "35d9a976-18d9-489d-9907-f08bc5251034",
          "type": "basic.input",
          "data": {
            "name": "c",
            "range": "[31:0]",
            "pins": [
              {
                "index": "31",
                "name": "",
                "value": ""
              },
              {
                "index": "30",
                "name": "",
                "value": ""
              },
              {
                "index": "29",
                "name": "",
                "value": ""
              },
              {
                "index": "28",
                "name": "",
                "value": ""
              },
              {
                "index": "27",
                "name": "",
                "value": ""
              },
              {
                "index": "26",
                "name": "",
                "value": ""
              },
              {
                "index": "25",
                "name": "",
                "value": ""
              },
              {
                "index": "24",
                "name": "",
                "value": ""
              },
              {
                "index": "23",
                "name": "",
                "value": ""
              },
              {
                "index": "22",
                "name": "",
                "value": ""
              },
              {
                "index": "21",
                "name": "",
                "value": ""
              },
              {
                "index": "20",
                "name": "",
                "value": ""
              },
              {
                "index": "19",
                "name": "",
                "value": ""
              },
              {
                "index": "18",
                "name": "",
                "value": ""
              },
              {
                "index": "17",
                "name": "",
                "value": ""
              },
              {
                "index": "16",
                "name": "",
                "value": ""
              },
              {
                "index": "15",
                "name": "",
                "value": ""
              },
              {
                "index": "14",
                "name": "",
                "value": ""
              },
              {
                "index": "13",
                "name": "",
                "value": ""
              },
              {
                "index": "12",
                "name": "",
                "value": ""
              },
              {
                "index": "11",
                "name": "",
                "value": ""
              },
              {
                "index": "10",
                "name": "",
                "value": ""
              },
              {
                "index": "9",
                "name": "",
                "value": ""
              },
              {
                "index": "8",
                "name": "",
                "value": ""
              },
              {
                "index": "7",
                "name": "",
                "value": ""
              },
              {
                "index": "6",
                "name": "",
                "value": ""
              },
              {
                "index": "5",
                "name": "",
                "value": ""
              },
              {
                "index": "4",
                "name": "",
                "value": ""
              },
              {
                "index": "3",
                "name": "",
                "value": ""
              },
              {
                "index": "2",
                "name": "",
                "value": ""
              },
              {
                "index": "1",
                "name": "",
                "value": ""
              },
              {
                "index": "0",
                "name": "",
                "value": ""
              }
            ],
            "virtual": true,
            "clock": false
          },
          "position": {
            "x": 200,
            "y": 480
          }
        },
        {
          "id": "e5fd73a0-3a3a-43d3-896d-e3261529bb0c",
          "type": "basic.code",
          "data": {
            "code": "//-- Numero de registros\nlocalparam NPOS = 32;\n\n  //-- Banco de registros\n  reg [31: 0] ram [0: NPOS-1];\n\n  //assign data_out = ram[addr[9:2]];\n  \n    //-- Lectura de la memoria \n  always @(posedge clk) begin\n  //if (read == 1)\n    if (da == 0)\n        a = 0;\n    else \n        a = ram[da];\n    if (db == 0)\n        b = 0;\n    else\n        b = ram[db];\n  end\n\n  //-- Escritura en la memoria\n  always @(posedge sync) begin\n    if (write == 1)\n     ram[dc] = c;\n  end\n",
            "params": [],
            "ports": {
              "in": [
                {
                  "name": "clk"
                },
                {
                  "name": "sync"
                },
                {
                  "name": "write"
                },
                {
                  "name": "da",
                  "range": "[4:0]",
                  "size": 5
                },
                {
                  "name": "db",
                  "range": "[4:0]",
                  "size": 5
                },
                {
                  "name": "dc",
                  "range": "[4:0]",
                  "size": 5
                },
                {
                  "name": "c",
                  "range": "[31:0]",
                  "size": 32
                }
              ],
              "out": [
                {
                  "name": "a",
                  "range": "[31:0]",
                  "size": 32
                },
                {
                  "name": "b",
                  "range": "[31:0]",
                  "size": 32
                }
              ]
            }
          },
          "position": {
            "x": 384,
            "y": 168
          },
          "size": {
            "width": 288,
            "height": 368
          }
        }
      ],
      "wires": [
        {
          "source": {
            "block": "43f394b7-622c-48fb-9456-31cd6a6c5953",
            "port": "out"
          },
          "target": {
            "block": "e5fd73a0-3a3a-43d3-896d-e3261529bb0c",
            "port": "clk"
          }
        },
        {
          "source": {
            "block": "0864bed1-5b93-4460-84d3-1d72f9e02ef6",
            "port": "out"
          },
          "target": {
            "block": "e5fd73a0-3a3a-43d3-896d-e3261529bb0c",
            "port": "sync"
          }
        },
        {
          "source": {
            "block": "ab957d18-156a-4ae5-a1c5-046f0d1663e1",
            "port": "out"
          },
          "target": {
            "block": "e5fd73a0-3a3a-43d3-896d-e3261529bb0c",
            "port": "da"
          },
          "size": 5
        },
        {
          "source": {
            "block": "47d8b235-fcc5-40f0-af14-17dfae40b975",
            "port": "out"
          },
          "target": {
            "block": "e5fd73a0-3a3a-43d3-896d-e3261529bb0c",
            "port": "write"
          }
        },
        {
          "source": {
            "block": "9adff702-e95d-4e15-94c5-d36cd38bb6ea",
            "port": "out"
          },
          "target": {
            "block": "e5fd73a0-3a3a-43d3-896d-e3261529bb0c",
            "port": "db"
          },
          "size": 5
        },
        {
          "source": {
            "block": "fba41b7e-6e66-4324-8b24-2cd4e2c4189a",
            "port": "out"
          },
          "target": {
            "block": "e5fd73a0-3a3a-43d3-896d-e3261529bb0c",
            "port": "dc"
          },
          "size": 5
        },
        {
          "source": {
            "block": "35d9a976-18d9-489d-9907-f08bc5251034",
            "port": "out"
          },
          "target": {
            "block": "e5fd73a0-3a3a-43d3-896d-e3261529bb0c",
            "port": "c"
          },
          "size": 32
        },
        {
          "source": {
            "block": "e5fd73a0-3a3a-43d3-896d-e3261529bb0c",
            "port": "a"
          },
          "target": {
            "block": "f6338ffc-3359-4dc2-b36c-727c79a0457a",
            "port": "in"
          },
          "size": 32
        },
        {
          "source": {
            "block": "e5fd73a0-3a3a-43d3-896d-e3261529bb0c",
            "port": "b"
          },
          "target": {
            "block": "4723d5cc-53f3-4ca3-8ff4-863ee4d017ba",
            "port": "in"
          },
          "size": 32
        }
      ]
    }
  },
  "dependencies": {}
}