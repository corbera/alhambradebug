{
  "version": "1.2",
  "package": {
    "name": "",
    "version": "",
    "description": "",
    "author": "",
    "image": ""
  },
  "design": {
    "board": "icezum",
    "graph": {
      "blocks": [
        {
          "id": "b765e164-48ec-4c4c-aee4-ace6e89c30c4",
          "type": "basic.input",
          "data": {
            "name": "",
            "pins": [
              {
                "index": "0",
                "name": "",
                "value": ""
              }
            ],
            "virtual": true,
            "clock": true
          },
          "position": {
            "x": -440,
            "y": -384
          }
        },
        {
          "id": "09bdfc04-d78d-41f2-91b5-e48b521f0d74",
          "type": "basic.input",
          "data": {
            "name": "sync",
            "pins": [
              {
                "index": "0",
                "name": "",
                "value": ""
              }
            ],
            "virtual": true,
            "clock": false
          },
          "position": {
            "x": -440,
            "y": -280
          }
        },
        {
          "id": "a041fd11-0903-4b55-9947-14446bb49908",
          "type": "basic.input",
          "data": {
            "name": "addr",
            "range": "[31:0]",
            "pins": [
              {
                "index": "31",
                "name": "",
                "value": ""
              },
              {
                "index": "30",
                "name": "",
                "value": ""
              },
              {
                "index": "29",
                "name": "",
                "value": ""
              },
              {
                "index": "28",
                "name": "",
                "value": ""
              },
              {
                "index": "27",
                "name": "",
                "value": ""
              },
              {
                "index": "26",
                "name": "",
                "value": ""
              },
              {
                "index": "25",
                "name": "",
                "value": ""
              },
              {
                "index": "24",
                "name": "",
                "value": ""
              },
              {
                "index": "23",
                "name": "",
                "value": ""
              },
              {
                "index": "22",
                "name": "",
                "value": ""
              },
              {
                "index": "21",
                "name": "",
                "value": ""
              },
              {
                "index": "20",
                "name": "",
                "value": ""
              },
              {
                "index": "19",
                "name": "",
                "value": ""
              },
              {
                "index": "18",
                "name": "",
                "value": ""
              },
              {
                "index": "17",
                "name": "",
                "value": ""
              },
              {
                "index": "16",
                "name": "",
                "value": ""
              },
              {
                "index": "15",
                "name": "",
                "value": ""
              },
              {
                "index": "14",
                "name": "",
                "value": ""
              },
              {
                "index": "13",
                "name": "",
                "value": ""
              },
              {
                "index": "12",
                "name": "",
                "value": ""
              },
              {
                "index": "11",
                "name": "",
                "value": ""
              },
              {
                "index": "10",
                "name": "",
                "value": ""
              },
              {
                "index": "9",
                "name": "",
                "value": ""
              },
              {
                "index": "8",
                "name": "",
                "value": ""
              },
              {
                "index": "7",
                "name": "",
                "value": ""
              },
              {
                "index": "6",
                "name": "",
                "value": ""
              },
              {
                "index": "5",
                "name": "",
                "value": ""
              },
              {
                "index": "4",
                "name": "",
                "value": ""
              },
              {
                "index": "3",
                "name": "",
                "value": ""
              },
              {
                "index": "2",
                "name": "",
                "value": ""
              },
              {
                "index": "1",
                "name": "",
                "value": ""
              },
              {
                "index": "0",
                "name": "",
                "value": ""
              }
            ],
            "virtual": true,
            "clock": false
          },
          "position": {
            "x": -440,
            "y": -184
          }
        },
        {
          "id": "dcd1c10f-521c-40c4-889a-0730b4d9a43f",
          "type": "basic.output",
          "data": {
            "name": "data_out",
            "range": "[31:0]",
            "pins": [
              {
                "index": "31",
                "name": "",
                "value": ""
              },
              {
                "index": "30",
                "name": "",
                "value": ""
              },
              {
                "index": "29",
                "name": "",
                "value": ""
              },
              {
                "index": "28",
                "name": "",
                "value": ""
              },
              {
                "index": "27",
                "name": "",
                "value": ""
              },
              {
                "index": "26",
                "name": "",
                "value": ""
              },
              {
                "index": "25",
                "name": "",
                "value": ""
              },
              {
                "index": "24",
                "name": "",
                "value": ""
              },
              {
                "index": "23",
                "name": "",
                "value": ""
              },
              {
                "index": "22",
                "name": "",
                "value": ""
              },
              {
                "index": "21",
                "name": "",
                "value": ""
              },
              {
                "index": "20",
                "name": "",
                "value": ""
              },
              {
                "index": "19",
                "name": "",
                "value": ""
              },
              {
                "index": "18",
                "name": "",
                "value": ""
              },
              {
                "index": "17",
                "name": "",
                "value": ""
              },
              {
                "index": "16",
                "name": "",
                "value": ""
              },
              {
                "index": "15",
                "name": "",
                "value": ""
              },
              {
                "index": "14",
                "name": "",
                "value": ""
              },
              {
                "index": "13",
                "name": "",
                "value": ""
              },
              {
                "index": "12",
                "name": "",
                "value": ""
              },
              {
                "index": "11",
                "name": "",
                "value": ""
              },
              {
                "index": "10",
                "name": "",
                "value": ""
              },
              {
                "index": "9",
                "name": "",
                "value": ""
              },
              {
                "index": "8",
                "name": "",
                "value": ""
              },
              {
                "index": "7",
                "name": "",
                "value": ""
              },
              {
                "index": "6",
                "name": "",
                "value": ""
              },
              {
                "index": "5",
                "name": "",
                "value": ""
              },
              {
                "index": "4",
                "name": "",
                "value": ""
              },
              {
                "index": "3",
                "name": "",
                "value": ""
              },
              {
                "index": "2",
                "name": "",
                "value": ""
              },
              {
                "index": "1",
                "name": "",
                "value": ""
              },
              {
                "index": "0",
                "name": "",
                "value": ""
              }
            ],
            "virtual": true
          },
          "position": {
            "x": 216,
            "y": -184
          }
        },
        {
          "id": "be32478c-c5a6-44b2-89f0-2932665a83c9",
          "type": "basic.input",
          "data": {
            "name": "data_in",
            "range": "[31:0]",
            "pins": [
              {
                "index": "31",
                "name": "",
                "value": ""
              },
              {
                "index": "30",
                "name": "",
                "value": ""
              },
              {
                "index": "29",
                "name": "",
                "value": ""
              },
              {
                "index": "28",
                "name": "",
                "value": ""
              },
              {
                "index": "27",
                "name": "",
                "value": ""
              },
              {
                "index": "26",
                "name": "",
                "value": ""
              },
              {
                "index": "25",
                "name": "",
                "value": ""
              },
              {
                "index": "24",
                "name": "",
                "value": ""
              },
              {
                "index": "23",
                "name": "",
                "value": ""
              },
              {
                "index": "22",
                "name": "",
                "value": ""
              },
              {
                "index": "21",
                "name": "",
                "value": ""
              },
              {
                "index": "20",
                "name": "",
                "value": ""
              },
              {
                "index": "19",
                "name": "",
                "value": ""
              },
              {
                "index": "18",
                "name": "",
                "value": ""
              },
              {
                "index": "17",
                "name": "",
                "value": ""
              },
              {
                "index": "16",
                "name": "",
                "value": ""
              },
              {
                "index": "15",
                "name": "",
                "value": ""
              },
              {
                "index": "14",
                "name": "",
                "value": ""
              },
              {
                "index": "13",
                "name": "",
                "value": ""
              },
              {
                "index": "12",
                "name": "",
                "value": ""
              },
              {
                "index": "11",
                "name": "",
                "value": ""
              },
              {
                "index": "10",
                "name": "",
                "value": ""
              },
              {
                "index": "9",
                "name": "",
                "value": ""
              },
              {
                "index": "8",
                "name": "",
                "value": ""
              },
              {
                "index": "7",
                "name": "",
                "value": ""
              },
              {
                "index": "6",
                "name": "",
                "value": ""
              },
              {
                "index": "5",
                "name": "",
                "value": ""
              },
              {
                "index": "4",
                "name": "",
                "value": ""
              },
              {
                "index": "3",
                "name": "",
                "value": ""
              },
              {
                "index": "2",
                "name": "",
                "value": ""
              },
              {
                "index": "1",
                "name": "",
                "value": ""
              },
              {
                "index": "0",
                "name": "",
                "value": ""
              }
            ],
            "virtual": true,
            "clock": false
          },
          "position": {
            "x": -440,
            "y": -88
          }
        },
        {
          "id": "d1e3b705-0b18-4c9a-90fb-2e8362781153",
          "type": "basic.input",
          "data": {
            "name": "write",
            "pins": [
              {
                "index": "0",
                "name": "",
                "value": ""
              }
            ],
            "virtual": true,
            "clock": false
          },
          "position": {
            "x": -440,
            "y": 16
          }
        },
        {
          "id": "73447917-b12f-4f4d-80ff-4e30114c5b73",
          "type": "basic.code",
          "data": {
            "code": "//-- Parametro: Nombre del fichero con el contenido de la RAM\nparameter RAMFILE = \"ram.list\";\n\n//-- Numero de posiciones totales de memoria\nlocalparam NPOS = 256;\n\n  //-- Memoria\n  reg [31: 0] ram [0: NPOS-1];\n\n  //assign data_out = ram[addr[9:2]];\n  \n    //-- Lectura de la memoria \n  always @(posedge clk) begin\n  //if (read == 1)\n    data_out = ram[addr[9:2]];\n  end\n\n  //-- Escritura en la memoria\n  always @(posedge sync) begin\n    if (write == 1)\n     ram[addr[9:2]] = data_in;\n  end\n\n//-- Cargar en la memoria el fichero ROMFILE\n//-- Los valores deben estan dados en hexadecimal\ninitial begin\n  $readmemh(RAMFILE, ram);\nend\n",
            "params": [],
            "ports": {
              "in": [
                {
                  "name": "clk"
                },
                {
                  "name": "sync"
                },
                {
                  "name": "addr",
                  "range": "[31:0]",
                  "size": 32
                },
                {
                  "name": "data_in",
                  "range": "[31:0]",
                  "size": 32
                },
                {
                  "name": "write"
                }
              ],
              "out": [
                {
                  "name": "data_out",
                  "range": "[31:0]",
                  "size": 32
                }
              ]
            }
          },
          "position": {
            "x": -248,
            "y": -400
          },
          "size": {
            "width": 360,
            "height": 496
          }
        }
      ],
      "wires": [
        {
          "source": {
            "block": "b765e164-48ec-4c4c-aee4-ace6e89c30c4",
            "port": "out"
          },
          "target": {
            "block": "73447917-b12f-4f4d-80ff-4e30114c5b73",
            "port": "clk"
          }
        },
        {
          "source": {
            "block": "a041fd11-0903-4b55-9947-14446bb49908",
            "port": "out"
          },
          "target": {
            "block": "73447917-b12f-4f4d-80ff-4e30114c5b73",
            "port": "addr"
          },
          "size": 32
        },
        {
          "source": {
            "block": "be32478c-c5a6-44b2-89f0-2932665a83c9",
            "port": "out"
          },
          "target": {
            "block": "73447917-b12f-4f4d-80ff-4e30114c5b73",
            "port": "data_in"
          },
          "size": 32
        },
        {
          "source": {
            "block": "d1e3b705-0b18-4c9a-90fb-2e8362781153",
            "port": "out"
          },
          "target": {
            "block": "73447917-b12f-4f4d-80ff-4e30114c5b73",
            "port": "write"
          }
        },
        {
          "source": {
            "block": "73447917-b12f-4f4d-80ff-4e30114c5b73",
            "port": "data_out"
          },
          "target": {
            "block": "dcd1c10f-521c-40c4-889a-0730b4d9a43f",
            "port": "in"
          },
          "size": 32
        },
        {
          "source": {
            "block": "09bdfc04-d78d-41f2-91b5-e48b521f0d74",
            "port": "out"
          },
          "target": {
            "block": "73447917-b12f-4f4d-80ff-4e30114c5b73",
            "port": "sync"
          }
        }
      ]
    }
  },
  "dependencies": {}
}