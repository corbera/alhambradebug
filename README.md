# README #
Repositorio con prueba de programa en Java de depuración serie de la placa IceZUM Alhambra, 
con ejemplos de circuitos básicos para hacer un procesador MIPS en dicha placa y con circuito 
para comunicación de la placa con el programa de depuración.

* **AlhambraSerial**: Proyecto Java en Netbeans con el código fuente del programa que se comunica por el puerto serie de la IceZUM Alhambra donde tiene que estar instalado el módulo DebugSerial de depuración

* **DebugSerial**: Módulo a utilizar en el circuito que carguemos en la placa para comunicarnos con el programa AlhambraSerial.

* **Ejemplos ICEStudio**: Diversos ejemplos de circuitos en ICEStudio para la Alhambra, entre los que encontramos ejemplo de uso del módulo DebugSerial y diversos circutos básicos del procesador MIPS

* **Coleccion MIPS para ICEStudio**: Colección para ICEStudio con los eleméntos básicos del procesador MIPS y el módulo DebugSerial.

* **Ejemplo de uso.mp4**: Vídeo con explicación breve del uso del módulo DebugSerial y del programa AlhambraSerial. 

![](Ejemplo_de_uso.gif)